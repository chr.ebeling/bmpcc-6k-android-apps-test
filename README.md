# BMPCC 6k Apps Test

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Motivation](#motivation)
- [Important notes](#important-notes)
- [Test conditions](#test-conditions)
- [Apps](#apps)
	- [Blackmagic BlueConnect](#blackmagic-blueconnect)
	- [Control Blackmagic Camera](#control-blackmagic-camera)
	- [Blackmagic Pocket Control 4k 6k](#blackmagic-pocket-control-4k-6k)
	- [Blackmagic Camera Control](#blackmagic-camera-control)

<!-- /TOC -->

## Motivation

The App [Blackmagic Camera Control](https://apps.apple.com/de/app/blackmagic-camera-control/id1239292847) from [Blackmagic Design](https://www.blackmagicdesign.com/) using Bluetooth to control nearly all of the BMPCC 6k features is only available for iPads. It's confusing, that an app with the same name from the developer [Cimus](https://play.google.com/store/apps/developer?id=Cimus&hl=de&gl=US) exists, but this app only supports to start or stop recording. We should not blame here Cimus for his/her work, because it's a free app and it always commendable, if someone spends his free time for such a project. It's rather a pity, that [Blackmagic Design](https://www.blackmagicdesign.com/) not provides also an Android App, because I'm convinced by all what I saw from Blackmagic Design, that they have great software developers. Until now I have not found any Android app controls all settings of BMPCC 6k, I started here to test promising apps.

Following tests will only include apps for Android. If developers reacts to this tests, I will also document their answers.

## Important notes

Please always be aware about the date of the test. If the test is long time ago, perhaps already a new version of App exists where all bugs already fixed. Following tests are not representative for all software versions.

## Test conditions
* *Camera:* Backmagic Pocket Cinema Camera 6k -> [link to manual](https://documents.blackmagicdesign.com/UserManuals/BlackmagicPocketCinemaCamera4KManual.pdf)
* *Mobile:* Samsung S20 / SM-G980F/DS

## Apps

### Blackmagic BlueConnect

First version of this app by [XBYTE Software Solutions](https://www.xbyte.sk/) seems to be very promising. Make a long story short: Install the [latest version 6.9.6 of Blachmagic Camera firmware](https://www.blackmagicdesign.com/de/support/family/professional-cameras) before you start using this app! I started with 6.9.5 and failed. The developer advices me to install fireware 6.9.6. In general this is a great app, works with all the features except the ones mentioned on [known issues](https://bmblueconnect.com/known-issues). More detailed test will follow.

* *Price:* 6.99€
* *Test date:* 24 March 2021
* [Google Play Store Link](https://play.google.com/store/apps/details?id=com.bmblueconnect.camcontrol)
* [Homepage](https://bmblueconnect.com/)
* *Sync values:* &#9989; Yes (WP,TINT, FPS, IRIS, SHUTTER, ISO, RAW, Auto Exposure, Frame Guide, Codec & Quality, ...)
* *Manual:* &#9940; No, but [Getting started](https://bmblueconnect.com/getting-started-android)
* *Blackmagic-firmware:* 6.9.6

Control features

| Feature | Works |
| --- | --- |
| WB | &#9989; Yes
| TINT | &#9989; Yes
| Presets WB | &#9989; Yes
| Frame Rate | &#9989; Yes
| Iris | &#9989; Yes
| Shutter | &#9989; Yes
| ISO | &#9989; Yes
| Auto Exposure | &#9989; Yes
| Focus | &#9989; Yes
| Focus sensitivity | &#9989; Yes
| Auto focus | &#9989; Yes
| Frame Guide | &#9989; Yes
| Safe area | &#9989; Yes
| Grids | &#9989; Yes
| Zebra (on/off) | &#9989; Yes
| Zebra (sensitivity) | &#9989; Yes
| Focus Assist level | &#9989; Yes
| Focus color | &#9989; Yes
| Focus Assist (on/of) | &#9989; Yes
| False color (on/of) | &#9989; Yes
| Switch between RAW/ProRes | &#9940; No, but [known issue](https://bmblueconnect.com/known-issues)
| Change quality inside RAW or ProRes | &#9989; Yes
| Change resolution inside RAW or ProRes | &#9989; Yes
| Audio Gain | &#9989; Yes
| Input audio (left) | &#9989; Yes
| Headphones | &#9989; Yes
| Speaker | &#9989; Yes
| Card changer | Not tested
| Meta-Scene | &#9989; Yes
| Meta-Lens | &#9989; Yes
| Meta-Project | &#9989; Yes


### Control Blackmagic Camera

* *Price:* 7.99€
* *Test date:* 24 March 2021
* *Blackmagic-firmware*:  6.9.6

App by [SayEffect Tech Solutions Private Limited](https://sayeffect.com/) seems to be not under development for BMPCC 6k, because otherwise there at least 6k as a option for resolution. More detailed test will follow.

### Blackmagic Pocket Control 4k 6k

* *Price:* 2.09€
* *Test date:* 24 March 2021
* *Blackmagic-firmware:* 6.9.6

Good price–performance ratio for the App by [The Federal Design Group](https://play.google.com/store/apps/developer?id=The+Federal+Design+Group). ***Rack Focus*** is a highlight. More detailed test will follow.

| Feature | Works |
| --- | --- |
| Iris | &#9989; Yes
| Shutter | &#9989; Yes
| WB | &#9989; Yes
| Tint | &#9989; Yes
| ISO | &#9989; Yes
| Frame Rate | &#9989; Yes
| Off Speed Recording | ?
| Sensor windowed | &#9940; No
| Zoom | ?
| Focus | &#9989; Yes
| Auto Focus | &#9989; Yes
| Rack Focus | &#9989; Yes
| Switch between RAW/ProRes | &#9940;
| Change quality inside RAW or ProRes | &#9989; Yes
| Change resolution inside RAW or ProRes | &#9989; Yes
| Meta-Scene | &#9989; Yes
| Meta-Lens | &#9989; Yes


### Blackmagic Camera Control

* For free
* *Test date:* 24 March 2021
* *Blackmagic-firmware:* 6.9.6

This free App by [Cimus](https://play.google.com/store/apps/developer?id=Cimus&hl=de&gl=US) makes no big promises. It correctly show basic setting values (once you found the correct Bluetooth ID of your camera), has a record button and it works.
